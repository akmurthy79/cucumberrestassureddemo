package com.anand.weather.util;


import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty","json:target/cucumber-json/cucumber.json",
                "junit:target/cucumber-reports/Cucumber.xml", "html:target/cucumber-reports"},
        glue = {"com.anand.weather.steps"},
        features={"src/test/resources/Weather.feature"}
)
public class CukeRunner {
}

