package com.anand.weather.util;

import com.anand.weather.model.ForecastRequest;
import com.anand.weather.model.response.WeatherResponseRoot;
import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class WeatherForecast {
    private final static Logger LOGGER = Logger.getLogger(WeatherForecast.class.getName());
    public static final String FORECAST_16_URL = "https://openweathermap.org/forecast16";

    ForecastRequest forecastRequest = new ForecastRequest();
    WeatherResponseRoot weatherResponseRoot = new WeatherResponseRoot();
    Gson gson = new Gson();
    Map<String, Double> dateTempMap = new HashMap<String, Double>();

    final RestAssuredConfig config = RestAssuredConfig.config().httpClient(HttpClientConfig.httpClientConfig()
            .setParam("http.connection.timeout", 900000)
            .setParam("http.socket.timeout", 900000));


    public void createReqForWeather(String cityName){
        //TODO  Download the cityID name json from and use GSON to map city name with the correct CityID
        // Recommended to look up based on City ID instead of City Name ( unique ID)
        if(cityName.compareToIgnoreCase("Sydney")==0){
            forecastRequest.setCityId("2147714");
            forecastRequest.setCityName(cityName);
            forecastRequest.setCount("23");
            forecastRequest.setAppId("836f9134941a77a1e718b7e4c25f87ec");
        }
        LOGGER.log(Level.INFO,"Created Request Object for Weather Forecast" + forecastRequest.getCityName());
    }

    public void requestForecast(){
        String ForecastForSydneyURL = createForecastURL();
        Response forecastResponse =
                RestAssured.given()
                        .config(config)
                        .when()
                        .get(ForecastForSydneyURL)
                        .then().
                        extract().response();

        weatherResponseRoot = gson.fromJson(forecastResponse.asString(), WeatherResponseRoot.class);
        LOGGER.log(Level.INFO,"REST GET call made for Weather Forecast" + forecastRequest.getCityName());

    }

    private String createForecastURL() {
        String ForecastForSydneyURL = "https://api.openweathermap.org/data/2.5/forecast?";
        ForecastForSydneyURL = ForecastForSydneyURL + "id=" +forecastRequest.getCityId();
        ForecastForSydneyURL = ForecastForSydneyURL + "&cnt=" +forecastRequest.getCount();
        ForecastForSydneyURL = ForecastForSydneyURL + "&appid=" +forecastRequest.getAppId();
        ForecastForSydneyURL = ForecastForSydneyURL + "&units=metric";
        return ForecastForSydneyURL;
    }

    public void extractMinTempForUpcomingThursday() {
        /*
         *  NOTE  With a free subscription on Open weather- 16 day forecasts are not available
         *  If we could get daily forecast for the next 16 days
         *  To check for weather for the upcoming Thursdays,
         *  Loop through the date stamps in the JSON response to check if the Date of Week value is
         *  5 for Thursday and then check the Min temp value
         */

        LOGGER.log(Level.INFO,"Step Skipped- Free subscription limitation for forecasting for 16 days in future");

    }

    public void mapForecastResponse(String tripLocation){
        for (int weatherIndex = 0; weatherIndex < weatherResponseRoot.getList().length; weatherIndex++) {
            dateTempMap.put(weatherResponseRoot.list[weatherIndex].getDt_txt(),
                    Double.valueOf(weatherResponseRoot.list[weatherIndex].getMain().getTemp_min()));
        }
        verifyForecastResponse(tripLocation);
        LOGGER.log(Level.INFO,"Extracting Time of Day and Temperature from REST response object");

    }

    private void verifyForecastResponse(String tripLocation) {
        boolean verifyResponse = (tripLocation.equalsIgnoreCase(weatherResponseRoot.getCity().getName())
                && (weatherResponseRoot.getList().length>0));
        Assert.assertTrue("Weather Forecast for "+tripLocation + " received is ", verifyResponse);
        LOGGER.log(Level.INFO,"Weather Forecast for "+tripLocation +" has been received.");

    }


    public void verifyTemperature(double degrees, String tripLocation){
        Assert.assertTrue("The temperature in "+ tripLocation +" is "+Collections.min(dateTempMap.values()) +
                " and is warmer than "+ degrees +" degrees", degrees< Collections.min(dateTempMap.values()));
        LOGGER.log(Level.INFO,"The temperature in "+ tripLocation +" is "+Collections.min(dateTempMap.values()) +
                " and is warmer than "+ degrees +" degrees" + (degrees< Collections.min(dateTempMap.values())));

    }

}
