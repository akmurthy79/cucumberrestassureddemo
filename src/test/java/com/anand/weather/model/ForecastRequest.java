package com.anand.weather.model;


import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ForecastRequest {
 private String cityId;
 private String appId;
 private String cityName;
 private String count;
 private String forecastDay;
}
