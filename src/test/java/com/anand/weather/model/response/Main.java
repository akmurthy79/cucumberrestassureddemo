package com.anand.weather.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Main
{
    private String temp;

    private String temp_min;

    private String grnd_level;

    private String temp_kf;

    private String humidity;

    private String pressure;

    private String sea_level;

    private String feels_like;

    private String temp_max;


}
