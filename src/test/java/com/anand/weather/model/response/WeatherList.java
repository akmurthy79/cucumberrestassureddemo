package com.anand.weather.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WeatherList
{
    private String dt;

    private String dt_txt;

    private Weather[] weather;

    private Main main;

    private Clouds clouds;

    private Sys sys;

    private Wind wind;


}