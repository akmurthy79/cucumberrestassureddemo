package com.anand.weather.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Weather
{
    private String icon;

    private String description;

    private String main;

    private String id;


}