package com.anand.weather.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Coord
{
    private String lon;

    private String lat;


}