package com.anand.weather.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class City

{
    private String country;

    private Coord coord;

    private String sunrise;

    private String timezone;

    private String sunset;

    private String name;

    private String id;


}