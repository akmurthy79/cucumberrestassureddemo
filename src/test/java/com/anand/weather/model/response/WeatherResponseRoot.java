package com.anand.weather.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WeatherResponseRoot
{
    private City city;

    private String cnt;

    private String cod;

    private String message;

    public WeatherList[] list;


}