package com.anand.weather.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Wind
{
    private String deg;

    private String speed;

}

