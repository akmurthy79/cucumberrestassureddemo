package com.anand.weather.steps;

import com.anand.weather.util.WeatherForecast;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

public class WeatherSteps {
    WeatherForecast weatherForecast = new WeatherForecast();
    String tripLocation;

    @Given("^I like to holiday in (.*)$")
    public void iLikeToHolidayInSydney(String locationOfTrip) {
        tripLocation = locationOfTrip;
        weatherForecast.createReqForWeather(tripLocation);
    }

    @And("^I only like to holiday on (.*)$")
    public void iOnlyLikeToHolidayOnThursdays(String dayOfTrip) {
        weatherForecast.extractMinTempForUpcomingThursday();
        /*
        Please, note that 16-days daily forecast and History API are not
        available for free subscribers
        */
    }

    @When("^I look up the weather forecast$")
    public void iLookUpTheWeatherForecast() {
        weatherForecast.requestForecast();
    }

    @Then("^I receive the weather forecast$")
    public void iReceiveTheWeatherForecast() {
        weatherForecast.mapForecastResponse(tripLocation);
    }

    @Then("the temperature is warmer than {double} degrees")
    public void theTemperatureIsWarmerThanDegrees(double degrees) {
        weatherForecast.verifyTemperature(degrees, tripLocation);
    }
}
