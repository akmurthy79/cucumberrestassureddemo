# cucumberRestAssuredDemo

Cucumber BDD automated Testing on https://openweathermap.org/api
For daily weather forecasting
**Note:** Free subscription for openweathermap allows daily forecasts only
(Forecast for 16 days in future comes with paid subscription)

**Getting Started**
To run this project locally
Git clone https://gitlab.com/akmurthy79/cucumberrestassureddemo.git

**Prerequisites**
Setup maven and JDK

**Run**
To run from Terminal:  mvn -Dtest=CukeRunner test

**Next Steps**
To use Json to POJO mapping for City Name to City ID mapping
(Currently City ID is hard- coded to map to City Name)